#!/bin/bash
# 
# simple example script to test runtime scaling
for i in 1 2 4 8 16 24; do
    rm -f ${fastqCacheDir}/*.fastq
    $DOCKER_SEQ/seq-analysis-tool.py --threads $i --do both --cd $RUN_LIST
done
